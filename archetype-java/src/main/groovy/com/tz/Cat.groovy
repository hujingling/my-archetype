package com.tz

import groovy.transform.ToString

/**
 * Created by hjl on 2016/11/29.
 */
@ToString(includeNames = true)
class Cat {

    String name
    int age

}
