# My-Archetype
## 背景
公司很多新手需要学习 maven，官网提供的 archetype 的版本和配置也有限。
新手自己配置 maven 存在不便，为了能让新手快速学习maven，
特提供各种内部常用的 archetype，供内部的新手学习使用。

## 使用方式

### 安装到本地开发机使用

1. 根据已存在的项目源码产生 archetype
`mvn archetype:create-from-project`

2. 分别将产生的文件整理至 archetype-resources 和 META-INF 目录

3. 本地安装 archetype
>  cd 至对应 archetype 目录 
 `mvn clean install archetype:update-local-catalog`

4. 本地使用 archetype
> cd 至需要创建新项目的工作区
执行指令，举例:
`mvn archetype:generate -DarchetypeGroupId=com.tz -DarchetypeArtifactId=archetype-web -DarchetypeVersion=1.0 -DarchetypeCatalog=local`


### 安装到公司 nexus 服务器使用

1. 上传公司内网 nexus 服务器
略

2. 测试服务器上的 archetype
格式: 
`mvn archetype:generate -DarchetypeGroupId=<foo> -DarchetypeArtifactId=<bar> -DarchetypeVersion=<版本号> -DarchetypeRepository=<网址>`
比如:
`mvn archetype:generate -DarchetypeGroupId=com.tz -DarchetypeArtifactId=archetype-web -DarchetypeVersion=1.0 -DarchetypeRepository=http://192.168.3.169:9002/nexus/content/groups/public`

