package com.tz;

import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.Test;

import javax.annotation.Resource;
import java.util.List;

/**
 * AppConfig Test
 */
@ContextConfiguration(classes = AppConfig.class)
public class AppConfigTest extends AbstractTestNGSpringContextTests {

    @Resource
    private List<String> myList;

    @Test
    public void test1() throws Exception {
        Assert.assertEquals(myList.size(), 1);
    }

}