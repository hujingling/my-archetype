package com.tz;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;

/**
 * AppConfig
 */
@Configuration // 当前类是配置信息类
public class AppConfig {

    @Bean
    public ArrayList myList() {
        ArrayList<String> list = new ArrayList<>();
        list.add("hello");
        return list;
    }

}
