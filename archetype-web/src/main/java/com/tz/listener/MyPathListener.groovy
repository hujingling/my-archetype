package com.tz.listener;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

/**
 * Application Path Listener
 */
@WebListener
class MyPathListener implements ServletContextListener {

    @Override
    void contextInitialized(ServletContextEvent servletContextEvent) {
        println "contextInitialized...."
        def application = servletContextEvent.getServletContext();
        def path = application.getContextPath();
        println "path: $path"
        application.setAttribute("path", path);
    }

    @Override
    void contextDestroyed(ServletContextEvent servletContextEvent) {

    }
}
