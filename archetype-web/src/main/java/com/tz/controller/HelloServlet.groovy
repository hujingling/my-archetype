package com.tz.controller

import javax.servlet.ServletException
import javax.servlet.annotation.WebServlet
import javax.servlet.http.HttpServlet
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

/**
 * Hello Servlet
 */
@WebServlet(urlPatterns = "/helloServlet")
class HelloServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.contentType = 'text/html;charset=UTF-8'
        response.writer.println('<h1>Hello Servlet</h1>')
    }
}
