package ${package}.listener;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

/**
 * Application Path Listener
 */
@WebListener
public class MyPathListener implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        System.out.println("contextInitialized....");
        ServletContext application = servletContextEvent.getServletContext();
        String path = application.getContextPath();
        System.out.println("path:" + path);
        application.setAttribute("path", path);
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {

    }
}
